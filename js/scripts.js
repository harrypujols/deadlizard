jQuery(document).ready(function($) {

//style the sidebar
$('#sidebar ul').addClass('side-nav');

//Start ↑ ↑ ↓ ↓ ← → ← → B  A
var contra = "http://www.harrypujols.com/";
var keys     = [];
    var konami  = '38,38,40,40,37,39,37,39,66,65,13';
	$(document)
        .keydown(
            function(e) {
                keys.push( e.keyCode );
                if ( keys.toString().indexOf( konami ) >= 0 ){
                    // do something when the code is executed
                 	//window.location = contra;
                 	alert('Easter egg to come soon.');
                    // empty the array containing the key sequence entered by the user
                    keys = [];
                }
            }
        );
//End ↑ ↑ ↓ ↓ ← → ← → B  A

//sticky footer
// Window load event used just in case window height is dependant upon images
$(window).bind("load", function() { 
       
       var footerHeight = 0,
           footerTop = 0,
           $footer = $("#footer");
           
       positionFooter();
       
       function positionFooter() {
       
                footerHeight = $footer.height();
                footerTop = ($(window).scrollTop()+$(window).height()-footerHeight)+"px";
       
               if ( ($(document.body).height()+footerHeight) < $(window).height()) {
                   $footer.css({
                        position: "absolute"
                   }).animate({
                        top: footerTop
                   })
               } else {
                   $footer.css({
                        position: "static"
                   })
               }
               
       }

       $(window)
               .scroll(positionFooter)
               .resize(positionFooter)
               
});
//end sticky footer
      
    
//foundation inits
 $(document).foundationMediaQueryViewer();
    
    $(document).foundationAlerts();
    $(document).foundationAccordion();
    $(document).tooltips();
    $('input, textarea').placeholder();
    
    
    
    $(document).foundationButtons();
    
    
    
    $(document).foundationNavigation();
    
    
    
    $(document).foundationCustomForms();
    
    
    
      
      $(document).foundationTabs({callback:$.foundation.customForms.appendCustomMarkup});
      
    
    
    
    $("#featured").orbit();

//end 
});
