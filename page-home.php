<?php get_header('home'); ?>

<div id="content" class="row">

 <section class="six columns centered">
<a href="#" data-reveal-id="home-content"><img id="logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/intro.gif" alt="Dead Lizard Graphics"></a> 
 </section>
</div><!--content-->
<div id="options" class="row">
	<section id="home-buttons" class="twelve columns centered">
				<div class="four columns">
	                 <a href="#" data-reveal-id="home-content"><b class="home-btn-about"></b></a>
				</div><!-- four columns -->
				<div class="four columns">
	                 <a href="<?php bloginfo('url'); ?>/blog"><b class="home-btn-blog"></b></a>
				</div><!-- four columns -->
			    <div class="four columns">
					 <div href="#" class="dropdown">
					  <a href="#" data-reveal-id="comics-list"><b class="home-btn-comics"></b></a>
					</div><!-- comics button -->
			    </div><!-- four columns -->
	</section>
</div><!-- options -->
					<!-- about window -->
 					<div id="home-content" class="reveal-modal">
					 	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
						<?php endwhile; else: ?>
						<p><?php bloginfo('description'); ?></p>
						<?php endif; ?>
						<a class="close-reveal-modal">&#215;</a>
					</div><!-- home-content -->
					
					<!-- comics list -->
					<?php
										//list terms in a given taxonomy using wp_list_categories
										$orderby = 'name';
										$show_count = 0; // 1 for yes, 0 for no
										$pad_counts = 0; // 1 for yes, 0 for no
										$hierarchical = 1; // 1 for yes, 0 for no
										$taxonomy = 'comics';
										$title = '';
										
										$args = array(
										  'orderby' => $orderby,
										  'show_count' => $show_count,
										  'pad_counts' => $pad_counts,
										  'hierarchical' => $hierarchical,
										  'taxonomy' => $taxonomy,
										  'title_li' => $title
										);
									?>
									<ul id="comics-list" class="reveal-modal">
									<?php wp_list_categories($args);?>
									<a class="close-reveal-modal">&#215;</a>
									</ul>
									
<?php get_footer('home'); ?>

<!-- Remember- Comics are best enjoyed with friends. -->
