<?

	//Add comics post type
add_action('init', 'comics_init');
function comics_init() {
	
	//Labels for dashboard
	$comic_labels = array(
	'name' => _x('Comics', 'post type general name'),
	'singular_name' => _x('Comic', 'post type singular name'),
	'all_items' => __('All comics pages'),
	'add_new' => _x('Add new comics page', 'comics'),
	'add_new_item' => __('Add new comics page'),
	'edit_item' => __('Edit comics page'),
	'view_item' => __('View comics page'),
	'search_items' => __('Search in comics pages'),
	'not_found' => __('No comics pages found'),
	'not_found_in_trash' => __('No comics pages found in trash'),
	'parent_item_colon' => ''
	);
	
	//Comics on dashboard
	$args = array(
	'description' => __( 'The comics site of Harry Pujols.' ),
	'labels' => $comic_labels,
	'public' => true,
	'publicly_queryable' => true,
	'show_ui' => true,
	'query_var' => true,
	'exclude_from_search' => false,
	'capability_type' => 'post',
	'hierarchical' => true,
	'menu_position' => 20,
	'menu_icon' => get_stylesheet_directory_uri() . '/images/comics-icon.png',
	'rewrite' => true,
	'supports' => array('title', 'editor', 'thumbnail')
	);
	register_post_type('pages', $args);
}

	//Comics folders
add_action('init', 'comics_stories', 0);

function comics_stories() {
	
	//labels for comics category
	$story_labels = array(
	'name' => _x('Comics', 'taxonomy general name'),
	'singular_name' => _x('Comic', 'taxonomy singular name'),
	'search_items' => __('Search in comics'),
	'all_items' => __('All comics'),
	'most_used_items' => null,
	'parent_item' => null,
	'parent_item_colon' => null,
	'edit_item' => __('Edit comic'),
	'update_item' => __('Update comic'),
	'add_new_item' => _('Add a new comic'),
	'new_item_name' => __('New comic'),
	'menu_name' => __('Comics')
	);
	
	//Custom comics category
	register_taxonomy('comics', 'pages', array(
	'hierarchical' => true,
	'labels' => $story_labels,
	'show_ui' => true,
	'query_var' => true,
	'rewrite' => true,
	));
}

?>