<?php get_header('comics'); ?>

<div id="container">
<span class="button" id="prev">&lsaquo;</span>
<div id="slideshow">
	<div id="scroller">
	  <ul id="slides">
 <!-- content begins here   -->   
  <!-- next line reverses the orders of the pages and sets the page limit to unlimited -->   
  
<?php global $query_string; query_posts($query_string . "&order=ASC&posts_per_page=-1"); ?>


 <!-- next lines publish the posts   -->  
 
 <?php remove_filter ('the_content', 'wpautop'); ?>
  
		<?php if (have_posts()) : ?>

			<?php while (have_posts()) : the_post(); ?>

							<li class="page"><?php the_content(); ?></li>
	
			<?php endwhile; ?>
	
	<?php else : ?>

		<h2>Huh? Where is the comic book?</h2>

	<?php endif; ?>
 
 <!-- content ends here   -->     
 
    
      </ul>
	</div><!--scroller-->
</div><!--slideshow-->
<span class="button" id="next">&rsaquo;</span>
</div><!--container-->

<ul id="indicator">
</ul>
<?php get_footer('comics'); ?>