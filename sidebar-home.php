<aside>
<ul class="nav-bar">
  <?php wp_list_pages('title_li='); ?> 
  
  <li class="has-flyout">
    <a href="#">Comics</a>
    <a href="#" class="flyout-toggle"><span> </span></a>
				<?php
					//list terms in a given taxonomy using wp_list_categories
					$orderby = 'name';
					$show_count = 0; // 1 for yes, 0 for no
					$pad_counts = 0; // 1 for yes, 0 for no
					$hierarchical = 1; // 1 for yes, 0 for no
					$taxonomy = 'comics';
					$title = '';
					
					$args = array(
					  'orderby' => $orderby,
					  'show_count' => $show_count,
					  'pad_counts' => $pad_counts,
					  'hierarchical' => $hierarchical,
					  'taxonomy' => $taxonomy,
					  'title_li' => $title
					);
				?>
				<ul class="flyout">
				<?php wp_list_categories($args);?>
				</ul>
  </li>
				    
</ul>
</aside>