<?php get_header(); ?>

<div id="content" class="row">

 <section class="eight columns">

<!-- Start the Loop. -->
 <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
 
  <div class="post">

 <h3><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>

 <div class="entry">
   <?php the_content(); ?>
 </div><!--entry-->

 <p class="postmetadata">Posted in <?php the_category(', '); ?></p>
 </div> <!-- post -->


 <?php endwhile; else: ?>

 <p>Oops. No posts! WTF?</p>

 <?php endif; ?>
 
 <nav>
 	<?php posts_nav_link(' &#8212; ', __('<span class="button">&laquo; Prev</class>'), __('<span class="button">Next &raquo;</span>')); ?>
 </nav>
 
 </section>
 
 <?php get_sidebar(); ?>
 
</div><!--content-->
<?php get_footer(); ?>

<!-- Remember- Comics are best enjoyed with friends. -->
